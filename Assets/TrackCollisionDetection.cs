using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackCollisionDetection : MonoBehaviour
{
    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.CompareTag("Dinosaur"))
        {
            collision.collider.GetComponent<DinoController>().DinoData.currentSpeed = 0;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Dinosaur"))
        {
            collision.collider.GetComponent<DinoController>().DinoData.currentSpeed = collision.collider.GetComponent<DinoController>().DinoData.Speed;
        }
    }
}
