using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggervuelta : MonoBehaviour
{
    [SerializeField]
    private GameObject Mitadvuelta, Meta; //Don�t forget place the GameObject for the triggers work 


    private void OnTriggerEnter()//active and deactivate half and last waypoint of the race 
    {
        Meta.SetActive(true);
        Mitadvuelta.SetActive(false);

    }
}
