using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAManager : MonoBehaviour
{
    private NavMeshAgent agent;
    private int CurrentWay;
    public Transform[] Waypoints;
    public Transform waypointsContainer;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        CurrentWay = 0;
        Waypoints = new Transform[waypointsContainer.childCount];//assinged the waypoints in the array so the Ia can Follow in in order 
        for (int i = 0; i < waypointsContainer.childCount; i++)
        {
            Waypoints[i] = waypointsContainer.GetChild(i);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        IAMove();
    }

    void IAMove()// IA movement trough the track waypoints placed in the tack prefab 
    {
        if (CurrentWay < Waypoints.Length && Vector3.Distance(transform.position, agent.destination) <= 5)
        {
            agent.destination = new Vector3(Waypoints[CurrentWay].position.x + UnityEngine.Random.Range(-10, 10), Waypoints[CurrentWay].position.y, Waypoints[CurrentWay].position.z + UnityEngine.Random.Range(-10, 10));
            CurrentWay++;
        }
        else if (CurrentWay == Waypoints.Length)
        {
            CurrentWay = 0;
        }

    }



}

