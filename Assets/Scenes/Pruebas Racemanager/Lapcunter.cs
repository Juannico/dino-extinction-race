using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lapcunter : MonoBehaviour
{
    [SerializeField]
    public  GameObject Metatrig,Mitadlap, minutos, segundos, miliseg,Lapcounter;
    public static int TotalLaps;


    private void OnTriggerEnter()// every lap the laptimecounter resets and save the time lap in the best textBox, also add the laps complete 
    {
        TotalLaps += 1;
        if (LapTimeManager.SecondCount <= 9)
        {
            segundos.GetComponent<Text>().text = "0" + LapTimeManager.SecondCount + ",";
        }
        else {
            segundos.GetComponent<Text>().text = "" + LapTimeManager.SecondCount + ",";
        }
        if (LapTimeManager.MinuteCount <= 9)
        {
            minutos.GetComponent<Text>().text = "0" + LapTimeManager.MinuteCount + ",";
        }
        else{
            minutos.GetComponent<Text>().text = "" + LapTimeManager.MinuteCount + ",";
        }
        miliseg.GetComponent<Text>().text = "" + LapTimeManager.MilliCount;

        LapTimeManager.MinuteCount = 0;
        LapTimeManager.SecondCount = 0;
        LapTimeManager.MinuteCount = 0;

        Lapcounter.GetComponent<Text>().text = "" + TotalLaps;
        Metatrig.SetActive(false);
        Mitadlap.SetActive(true);
        Debug.Log(TotalLaps);
                    
    }
}
