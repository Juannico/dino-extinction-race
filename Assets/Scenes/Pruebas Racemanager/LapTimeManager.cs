﻿using UnityEngine;
using UnityEngine.UI;
//this script makes the race time work
public class LapTimeManager : MonoBehaviour
{
    public static int MinuteCount, SecondCount;
    public static float MilliCount;
    public static string MilliDisplay;
    public GameObject MinutosUI, SegundosUI, MiliUI;// this are the texts box 

    
    void Update()// this script manage the timers in the screnn make a simple timer whit time.deltatime. 
    {
        MilliCount += Time.deltaTime * 10;
        MilliDisplay = MilliCount.ToString("F0");
        MiliUI.GetComponent<Text>().text = "" + MilliDisplay;

        if (MilliCount >= 10)
        {
            MilliCount = 0;
            SecondCount += 1;
        }

        if (SecondCount <= 9)
        {
            SegundosUI.GetComponent<Text>().text = "0" + SecondCount + ".";
        }
        else
        {
            SegundosUI.GetComponent<Text>().text = "" + SecondCount + ".";
        }

        if (SecondCount >= 60)
        {
            SecondCount = 0;
            MinuteCount += 1;
        }

        if (MinuteCount <= 9)
        {
            MinutosUI.GetComponent<Text>().text = "0" + MinuteCount + ":";
        }
        else
        {
            MinutosUI.GetComponent<Text>().text = "" + MinuteCount + ":";
        }
    }
}