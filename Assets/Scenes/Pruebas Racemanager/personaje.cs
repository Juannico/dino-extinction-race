using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personaje : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField]
    float Velocidad, velgiro, Gravedad;
    private bool isgrounded = true;
    [SerializeField]
    private Animator An;
    Vector3 jump;



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        An = GetComponent<Animator>();
        
    }

    public void FixedUpdate()
    {
        //makes all the logic of movement 
        Mover();
        Turn();
        Caida();
        Jump();
      

    }

    void Mover()// moves forward and backward and set the value for move blend tree 
    {


        if (Input.GetKey(KeyCode.W))
        {
            rb.AddRelativeForce(new Vector3(Vector3.forward.x, 0, Vector3.forward.z) * Velocidad * 10);
            // vel += Velocidad + Time.deltaTime;

        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.AddRelativeForce(new Vector3(Vector3.forward.x, 0, Vector3.forward.z) * -Velocidad * 10);
            //  vel -= Velocidad + Time.deltaTime;
        }
        Vector3 Velocidadlocal = transform.InverseTransformDirection(rb.velocity);
        Velocidadlocal.x = 0;
        rb.velocity = transform.TransformDirection(Velocidadlocal);
        //An.SetFloat("Velocidad", rb.velocity.magnitude);
    }

    void Turn()// simulte the turn and torque need to make the dinasours turn, all the values can be adjust in the inspector 
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddTorque(Vector3.up * velgiro * 10);

        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            rb.AddTorque(-Vector3.up * velgiro * 10);

        }
    }

    void Jump() // makes the jump when checks if it is grounded and run the animations. 
    {
        if (Input.GetKeyDown(KeyCode.Space) && isgrounded)
        {
            rb.AddForce(jump = new Vector3(0, Gravedad, 0), ForceMode.Impulse);
            isgrounded = false;
            An.SetBool("Jump", true);
                     
        }
        //else
        //An.SetBool("Jump", false);

    }
    private void OnCollisionEnter(Collision collision)// Check if the dino is grounded to avoid jumping twice
    {
        if (collision.gameObject.tag == "Track")
        {
            isgrounded = true;

        }

    }
    void Caida()//replaces the gravity of the rigidbody in case it is deactivated
    {
        rb.AddForce(Vector3.down * Gravedad);
       
    }


}

