using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceMan : MonoBehaviour
{
    [SerializeField]
    public GameObject Countdown, player, IA, LapTimer, Finrace;
    public AudioSource Countsound, Startsound;
    [SerializeField]
    private GameObject[] dinosprefb;
    [SerializeField]
    private GameObject Playerprefb;
    [SerializeField]
    private Transform[] PosIA;
    private GameObject dinosIA;
    [SerializeField]
    private Transform PosPlayer;
    private GameObject Player;
    public Camera Cam;
    void Start()// set all the inicial conditions for the race can work 
    {

        player.GetComponent<personaje>().enabled = false;
        IA.GetComponent<IAManager>().enabled = false;
        StartCoroutine(Contador());
        AddRacer();
        Finrace.SetActive(false);
        LapTimer.SetActive(false);

    }


    void Update()
    {
        if (Lapcunter.TotalLaps >= 3)
        {
            Finrace.SetActive(true);
        }


    }

    IEnumerator Contador()// start the countdown timer for the race start and activate the movement of the racers 
    {
        yield return new WaitForSeconds(0.5f);

        Countdown.GetComponent<Text>().text = "3";
        Countsound.Play();
        Countdown.SetActive(true);
        yield return new WaitForSeconds(1f);
        Countdown.SetActive(false);
        Countdown.GetComponent<Text>().text = "2";
        Countdown.SetActive(true);
        yield return new WaitForSeconds(1f);
        Countdown.SetActive(false);
        Countdown.GetComponent<Text>().text = "1";
        Countdown.SetActive(true);
        yield return new WaitForSeconds(1f);
        Countdown.SetActive(false);
        Startsound.Play();
        player.GetComponent<personaje>().enabled = true;
        IA.GetComponent<IAManager>().enabled = true;
        LapTimer.SetActive(true);
    }

    public void AddRacer() // add all the racers to the race. The player and the IA in the star positions  
    {
        for (int i = 0; i < dinosprefb.Length; i++)
        {
            Instantiate(dinosprefb[i], PosIA[i].transform.position, PosIA[i].transform.rotation);
        }

        player = Instantiate(player, PosPlayer.transform.position, PosPlayer.transform.rotation);
    }

}
