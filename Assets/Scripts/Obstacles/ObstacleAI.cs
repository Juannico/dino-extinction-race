﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class ObstacleAI : MonoBehaviour
{
    public Transform[] waypoints;
    private NavMeshAgent agent;
    private int currentW = 0;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.destination = waypoints[0].position;
    }

    // Update is called once per frame
    void Update()
    {
        if ((!agent.hasPath || agent.remainingDistance <= 2f) && currentW < waypoints.Length)
        {
            agent.destination = waypoints[currentW].position;
            currentW++;
        }
        if (currentW == waypoints.Length)
        {
            currentW = 0;
        }
    }
}
