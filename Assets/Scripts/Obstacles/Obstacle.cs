﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Obstacle : MonoBehaviour
{
    [SerializeField] private LayerMask collidingLayer;
    [SerializeField] private AudioClip collisionSound;
    [SerializeField] private GameObject particleSystemGameobject;

    protected AudioSource obstacleAudioSource;
    protected ParticleSystem collisionParticles;
    // Start is called before the first frame update
    void Start()
    {
        obstacleAudioSource = gameObject.AddComponent<AudioSource>();
        obstacleAudioSource.clip = collisionSound;
        if(particleSystemGameobject)
            collisionParticles = Instantiate(particleSystemGameobject, transform.position, Quaternion.identity, transform).GetComponent<ParticleSystem>();
    }

    public virtual void OnCollisionWithObstacle(GameObject gameobjectCollided)
    {
        obstacleAudioSource.Play();
        collisionParticles?.Play();
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if(((1 << collision.gameObject.layer) & collidingLayer) != 0) OnCollisionWithObstacle(collision.gameObject);
    }
}
