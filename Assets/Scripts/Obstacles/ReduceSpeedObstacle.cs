using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReduceSpeedObstacle : Obstacle
{
    [Tooltip("Reduces the speed by a factor between 0 and 1, where 0 reduces nonme and 1 reduces the full speed")]
    [Range(0, 1)]
    [SerializeField] private float reduceByFactor; 
    public override void OnCollisionWithObstacle(GameObject gameobjectCollided)
    {
        base.OnCollisionWithObstacle(gameobjectCollided);
        gameobjectCollided.GetComponent<DinoController>().DinoData.currentAcceleration *= 1 - reduceByFactor;
    }
}
