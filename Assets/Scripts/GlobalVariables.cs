using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGlobalVariables", menuName = "DinoExtRace/Create Global Variables", order = 52)]
public class GlobalVariables : ScriptableObject
{
    [SerializeField] public float GroundCheckDistance = 0.3f;
    [SerializeField] public float GravityMultiplier = 1.5f;
}
