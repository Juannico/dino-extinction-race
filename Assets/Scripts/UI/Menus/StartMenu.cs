using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenu : BaseMenu
{
    // Start is called before the first frame update
    void Start()
    {
        inputActions.PlayerActions.Start.performed += (ctx) => GoToMenu(nextMenu);
    }
}
