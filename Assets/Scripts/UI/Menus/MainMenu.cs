using UnityEngine;
using UnityEngine.EventSystems;


public class MainMenu : BaseMenu
{
    [Header("Menu Buttons")]

    [SerializeField] private EasyTweenButton singlePlayerButton;
    [SerializeField] private EasyTweenButton multiplayerButton;
    [SerializeField] private EasyTweenButton optionsButton;
    [SerializeField] private EasyTweenButton creditsButton;


    [Header("Menus")]
    [SerializeField] private GameObject raceTypeMenu;
    [SerializeField] private GameObject creditsMenu;
    // Start is called before the first frame update
    void Start()
    {
        singlePlayerButton.onClick.AddListener(() => GoToMenu(raceTypeMenu));
        multiplayerButton.onClick.AddListener(() => GoToMenu(raceTypeMenu));
        optionsButton.onClick.AddListener(() => GoToMenu(nextMenu));
        creditsButton.onClick.AddListener(() => GoToMenu(creditsMenu));
        inputActions.UI.Cancel.performed += (ctx) => GoToMenu(previousMenu);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        singlePlayerButton.Select();
    }
}
