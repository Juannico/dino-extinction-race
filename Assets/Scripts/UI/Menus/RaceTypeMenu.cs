using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RaceTypeMenu : BaseMenu
{
    [Header("Race Type Menu Buttons")]
    [SerializeField] private EasyTweenButton grandPrixButton;
    [SerializeField] private EasyTweenButton vsRaceButton;

    void Start()
    {
        grandPrixButton.onClick.AddListener(() =>
        {
            GoToMenu(nextMenu);
            gameDataReference.SelectedGameMode = GameMode.CUP;
        });
        vsRaceButton.onClick.AddListener(() =>
        {
            GoToMenu(nextMenu);
            gameDataReference.SelectedGameMode = GameMode.SINGLE_RACE;
        }); 
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        grandPrixButton.Select();
    }
}
