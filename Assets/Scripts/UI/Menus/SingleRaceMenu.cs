using MagneticScrollView;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.UI;

public class SingleRaceMenu : BaseMenu
{
    [SerializeField] private GameObject firstRaceButton;
    [SerializeField] private MagneticScrollRect magneticScroll;

    protected override void OnEnable()
    {
        base.OnEnable();
        StartCoroutine(CoroutineHandler.ExecuteActionAfter(() => firstRaceButton.GetComponent<EasyTweenButton>()?.Select(), .2f));
    }
}
