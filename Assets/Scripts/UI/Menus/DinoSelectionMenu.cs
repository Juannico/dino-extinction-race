using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class DinoSelectionMenu : BaseMenu
{
    //Might not be neccesary as we just need to know the first button to select
    [SerializeField] private DinoSelectionButton[] dinoButtonList;
    [SerializeField] private PlayerMenuController[] playerMenuControllers;
    [Header("Possible Next Menus")]
    [SerializeField] private GameObject cupMenuGameobjectReference;
    [SerializeField] private GameObject singleRaceMenuGameobjectReference;
    // Start is called before the first frame update
    void Start()
    {
        //Overrideing the cancel button press
        inputActions.UI.Cancel.Reset();
        inputActions.UI.Cancel.performed += OnBackPressed;
        //HARD CODE FOR THE FIRST PLAYER
        playerMenuControllers[0].Initialize(dinoButtonList);
        playerMenuControllers[0].OnDinoSelected += GoToMenuOnGameMode;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        playerMenuControllers[0].gameObject.SetActive(true);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        if (playerMenuControllers[0] == null) return;

        playerMenuControllers[0].gameObject.SetActive(false);
    }

    private void OnBackPressed(InputAction.CallbackContext obj)
    {
        if (!playerMenuControllers[0].HasSelected)
        {
            GoToMenu(previousMenu);
            return;
        }
        playerMenuControllers[0].DeselectDino();
    }

    private void GoToMenuOnGameMode()
    {
        switch (gameDataReference.SelectedGameMode)
        {
            case GameMode.CUP:
                nextMenu = cupMenuGameobjectReference;
                break;
            case GameMode.SINGLE_RACE:
                nextMenu = singleRaceMenuGameobjectReference;
                break;
            default:
                nextMenu = cupMenuGameobjectReference;
                break;
        }

        GoToMenu(nextMenu);
    }
}
