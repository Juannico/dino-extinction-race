using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CupMenu : BaseMenu
{
    [SerializeField] private EasyTweenButton firstCupButton;
    //Basically copy paste of what is in the game manager but should be changed for something more robust down the line
    public CupData firstCup;

    // Start is called before the first frame update
    void Start()
    {
        firstCupButton.onClick.AddListener(() =>
        {
            gameDataReference.SelectedCup = firstCup;
            LoadingScreenManager.LoadScene(firstCup.MapsIndexes[0]);
            gameDataReference.SelectedGameMode = GameMode.CUP;
        });
        firstCupButton.Select();
    }
}
