using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum GameMode
{
    CUP = 0,
    SINGLE_RACE
}

public class BaseMenu : MonoBehaviour
{
    [Header("Base Menu")]
    protected PlayerInputActions inputActions;
    [SerializeField] private Image fadeInImage;
    [SerializeField] protected GameObject nextMenu;
    [SerializeField] protected GameObject previousMenu;

    protected GameData gameDataReference;

    private void Awake()
    {
        inputActions = new PlayerInputActions();
        gameDataReference = Resources.Load("GameData") as GameData;
        inputActions.UI.Cancel.performed += (ctx) => GoToMenu(previousMenu);
    }

    protected virtual void Update()
    {
        //Empty
    }

    protected virtual void OnEnable()
    {
        inputActions.Enable();
    }

    protected virtual void OnDisable()
    {
        inputActions.Disable();  
    }

    public void GoToMenu(GameObject menuToLoad)
    {
        StartCoroutine(MenuTransition(menuToLoad));
    }

    IEnumerator MenuTransition(GameObject toActivate)
    {
        fadeInImage.GetComponent<EasyTween>().ChangeSetState(false);
        fadeInImage.GetComponent<EasyTween>().OpenCloseObjectAnimation();
        yield return new WaitForSeconds(fadeInImage.GetComponent<EasyTween>().GetAnimationDuration());
        toActivate.SetActive(true);
        fadeInImage.GetComponent<EasyTween>().OpenCloseObjectAnimation();
        gameObject.SetActive(false);
    }

    public void LoadAnotherSceneAfterFade(string sceneName)
    {
        fadeInImage.GetComponent<EasyTween>().ChangeSetState(false);
        fadeInImage.GetComponent<EasyTween>().OpenCloseObjectAnimation();
        float duration = fadeInImage.GetComponent<EasyTween>().GetAnimationDuration();
        StartCoroutine(CoroutineHandler.ExecuteActionAfter(() =>
        {
            LoadingScreenManager.LoadSceneByName(sceneName);

        }, duration));
    }
}
