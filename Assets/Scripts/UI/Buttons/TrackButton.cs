using UnityEngine;
using UnityEngine.UI;

public class TrackButton : MonoBehaviour 
{
    public void Select()
    {
        GetComponent<EasyTween>()?.ChangeSetState(false);
        GetComponent<EasyTween>()?.OpenCloseObjectAnimation();
    }
}
