using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;


public class EasyTweenButton : Selectable, ISubmitHandler
{
    public GameObject Outline;
    public UnityEvent onClick;
    public UnityEvent onSelected;
    public UnityEvent onDeselected;
    //Both overrides of select have to call the custom select.
    //This due to select being used on screen transitioning and on select when using default nabigation
    //For some reason both have to call the method or it will break at some point
    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        CustomSelect();
    }

    public override void Select()
    {
        base.Select();
        CustomSelect();
    }

    private void CustomSelect()
    {
        if (Outline) Outline.GetComponent<Image>().material.SetFloat("_Rainbow", 1);
        GetComponent<EasyTween>()?.ChangeSetState(false);
        GetComponent<EasyTween>()?.OpenCloseObjectAnimation();
        onSelected?.Invoke();
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        if (Outline) Outline.GetComponent<Image>().material.SetFloat("_Rainbow", 0);
        GetComponent<EasyTween>()?.ChangeSetState(true);
        GetComponent<EasyTween>()?.OpenCloseObjectAnimation();
        onDeselected?.Invoke();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        onClick?.Invoke();
    }

}
