using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DinoSelectionButton : Selectable
{
    [SerializeField] private Dinosaur dinoData;
    //Might have to change this becuse this only works with one player
    [SerializeField] private Image playerSelectionImage;

    public PlayerMenuController PlayerHovering { get; set; }

    public Dinosaur DinoData { get => dinoData; }

    protected override void OnEnable()
    {
        base.OnEnable();
        playerSelectionImage.enabled = false;
        image.sprite = dinoData.DinoIcon;
    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        playerSelectionImage.enabled = true;
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        playerSelectionImage.enabled = false;
        PlayerHovering.DeselectDino();
    }

}
