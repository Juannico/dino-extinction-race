﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public static int START_RACE = 1;
    public static int DURING_RACE = 2;
    public static int END_RACE = 3;
    public static int OUT_OF_RACE = 4;
    public int localNumber;

    public Text lap, pos, finalpos;
    [Header("Wrong Way Text")]
    public GameObject wrongWayText;
    [Header("Poder")]
    public Sprite[] poderes;
    public Image poder;
    public Image visionBlock;
    public Text pointsGained;
    public Text totalPoints;

    private GameObject playerDino;
    private int vueltaPlayer;
    private int raceStage;
    private RaceManager raceM;
    DinoController dinoPlayer;
    GameData gameDataReference;

    [SerializeField] private Text countdownText; //Maybe look into changing to TextMeshPro

    private void Start()
    {
        //gameDataReference = Resources.Load("GameData") as GameData;
        //raceM = FindObjectOfType<RaceManager>();
        //raceStage = START_RACE;
        //poder.transform.parent.gameObject.SetActive(true);
        //lap.gameObject.SetActive(true);
        //pos.gameObject.SetActive(true);
        //for (int i = 0; i < raceM.m_Dinos.Count; i++)
        //{
        //    if (raceM.m_Dinos[i].GetComponent<PlayerController>().localNumber == localNumber)
        //    {
        //        raceM.m_Dinos[i].isPlayerControlled = true;
        //        playerDino = raceM.m_Dinos[i].gameObject;
        //        dinoPlayer = raceM.m_Dinos[i];
        //        dinoPlayer.ReferenceHUD = this;
        //        break;
        //    }
        //}
        //finalpos.text = "";
    }

    private void Update()
    {
        //if (playerDino != null)
        //{
        //    vueltaPlayer = dinoPlayer.CurrentLap + 1;
        //    lap.text = "Lap: " + vueltaPlayer + "/3";
        //    if (raceStage == START_RACE)
        //    {
        //        if (raceM.StartRace())
        //        {
        //            raceStage = DURING_RACE;
        //        }
        //    }
        //    if (raceStage == DURING_RACE)
        //    {
        //        if (!playerDino.GetComponent<PlayerController>().enabled)
        //        {
        //            playerDino.GetComponent<PlayerController>().enabled = true;
        //        }

        //        if (dinoPlayer.PowerStored)
        //        {
        //            poder.sprite = poderes[dinoPlayer.PowerStored.powerNumber];
        //        }
        //        else
        //        {
        //            poder.sprite = poderes[poderes.Length-1];
        //        }

        //        if (dinoPlayer.RacePostion == 1)
        //        {
        //            pos.text = "1st";
        //        }
        //        else if (dinoPlayer.RacePostion == 2)
        //        {
        //            pos.text = "2nd";
        //        }
        //        else if (dinoPlayer.RacePostion == 3)
        //        {
        //            pos.text = "3rd";
        //        }
        //        else if (dinoPlayer.RacePostion > 3)
        //        {
        //            pos.text = dinoPlayer.RacePostion.ToString() + "th";
        //        }

        //        if (vueltaPlayer == 4)
        //        {
        //            raceStage = END_RACE;
        //        }
        //    }
        //    else if (raceStage == END_RACE)
        //    {
        //        //Activate the AI controller so that the dinosaur starts moving on its own
        //        playerDino.GetComponent<AIController>().enabled = true;
        //        playerDino.GetComponent<PlayerController>().enabled = false;
        //        //show point gained and Points total if in cup
        //        if (gameDataReference.SelectedCup != null)
        //        {
        //            pointsGained.gameObject.SetActive(true);
        //            totalPoints.gameObject.SetActive(true);
        //            int tempPoint = raceM.AwardPoints(dinoPlayer.RacePostion, localNumber);
        //            gameDataReference.CupPoints += tempPoint;
        //            dinoPlayer.CupPoints += tempPoint;
        //            pointsGained.text = "+ " + tempPoint;
        //            totalPoints.text = raceM.TotalPlayerPoints(dinoPlayer).ToString();
        //        }
        //        //Wait for everybody to finish the race
        //        poder.transform.parent.gameObject.SetActive(false);
        //        lap.gameObject.SetActive(false);
        //        pos.gameObject.SetActive(false);
        //        finalpos.text = pos.text;
        //        raceStage = OUT_OF_RACE;
        //        raceM.Finishers++;
        //    }
        //}

    }

    public void ChangeCountdownText(int currentCountdownTime)
    {
        countdownText.text = currentCountdownTime.ToString();
        if (currentCountdownTime <= 0)
        {
            countdownText.text = "Go!";

            StartCoroutine(CoroutineHandler.ExecuteActionAfter(
                () => countdownText.gameObject.SetActive(false)
                , 1f));
        }
    }
}
