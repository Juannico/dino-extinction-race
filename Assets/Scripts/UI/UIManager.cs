﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    /*
     * The objects on the race UI
     */
    [Header("Race Gameobjects")]
    public GameObject pauseMenu;
    public Button buttonPause;
    public Text countdown;
    /*
     * Position Panel
     */
    public GameObject posPanel;

    private bool isPaused;
    public int playerPause;

    /*
     * This method is used in multiples buttons to load different scenes
     */
    public void LoadScene(int sceneIndex)
    {
        LoadingScreenManager.LoadScene(sceneIndex);
    }

    public void ChangeCountdownText(string newT)
    {
        countdown.text = newT;
    }

    public void loadPanelPositions(List<DinoController> dinosT)
    {
        for (int i = 0; i < dinosT.Count; i++)
        {
            posPanel.transform.GetChild(i).gameObject.SetActive(true);
            posPanel.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = dinosT[i].RacePostion.ToString();
            //posPanel.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = dinosT[i].dinoName;
        }
        posPanel.SetActive(true);
    }

    public void PauseActions(int playerP)
    {
        if (!isPaused)
        {
            pauseMenu.SetActive(true);
            isPaused = true;
            playerPause = playerP;
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
            isPaused = false;
            playerPause = 0;
        }
    }
}
