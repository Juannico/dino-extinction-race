﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerMenuController : MonoBehaviour
{

    private GameData gameDataReference;

    [Tooltip("Amount of time between dino selection change")]
    public float repeatDelay;
    private float lastUpdate;
    public GameObject selectBox;

    private GameObject selectedDino;
    public Text dinoNamePlaceholder;
    public GameObject dinoNameGameObject;
    public GameObject platform;
    public DinoSelectionButton[] dinoButtons;
    private DinoSelectionButton hoveredDinoButton;
    public delegate void OnSubmitPerformed();
    public OnSubmitPerformed OnDinoSelected;

    private PlayerInputActions subInputActions;

    private Vector2 dinoMenuNavigationVector;
    public bool HasSelected { get; private set; }

    private void Awake()
    {
        gameDataReference = Resources.Load("GameData") as GameData;
        subInputActions = new PlayerInputActions();  
        subInputActions.UI.Submit.performed += ConfirmSelection;
        subInputActions.UI.Navigate.performed += (ctx) => { dinoMenuNavigationVector = ctx.ReadValue<Vector2>(); };
    }


    // Start is called before the first frame update
    public void Initialize(DinoSelectionButton[] buttonArray)
    {
        platform.SetActive(true);
        dinoNameGameObject.SetActive(true);
        selectBox.SetActive(true);
        //Populate array
        dinoButtons = new DinoSelectionButton[buttonArray.Length];
        buttonArray.CopyTo(dinoButtons, 0);
        HoverDino();
    }

    private void HoverDino()
    {
        if (dinoButtons.Length <= 0) return;

        dinoButtons[0].Select();
        hoveredDinoButton = dinoButtons[0];
        hoveredDinoButton.PlayerHovering = this;
        SelectDino();
    }

    private void OnEnable()
    {
        subInputActions.Enable();
        HoverDino();
    }

    private void OnDisable()
    {
        subInputActions.Disable();
        if (selectedDino)
            DestroyImmediate(selectedDino);
    }

    //// Update is called once per frame
    //// This can be easily better formatted for more clarity
    void Update()
    {
        if (lastUpdate > Time.time - repeatDelay) return;
        if (dinoMenuNavigationVector == Vector2.zero) return;
        if (dinoMenuNavigationVector.x > 0)
        {
            if ((DinoSelectionButton)hoveredDinoButton.FindSelectableOnRight() == null) return;
            hoveredDinoButton = (DinoSelectionButton)hoveredDinoButton.FindSelectableOnRight();
        }
        else if (dinoMenuNavigationVector.x < 0)
        {
            if ((DinoSelectionButton)hoveredDinoButton.FindSelectableOnLeft() == null) return;
            hoveredDinoButton = (DinoSelectionButton)hoveredDinoButton.FindSelectableOnLeft();
        }

        if (dinoMenuNavigationVector.y > 0)
        {
            if ((DinoSelectionButton)hoveredDinoButton.FindSelectableOnUp() == null) return;
            hoveredDinoButton = (DinoSelectionButton)hoveredDinoButton.FindSelectableOnUp();
        }
        else if (dinoMenuNavigationVector.y < 0)
        {
            if ((DinoSelectionButton)hoveredDinoButton.FindSelectableOnDown() == null) return;
            hoveredDinoButton = (DinoSelectionButton)hoveredDinoButton.FindSelectableOnDown();
        }
        hoveredDinoButton.Select();
        hoveredDinoButton.PlayerHovering = this;
        SelectDino();
        lastUpdate = Time.time;
    }

    private void SelectDino()
    {
        selectedDino = Instantiate(hoveredDinoButton.DinoData.DinoMenuPrefab, transform.position, Quaternion.Euler(0, 180, 0), transform);
        dinoNamePlaceholder.text = hoveredDinoButton.DinoData.DinoName;
    }

    private void LateUpdate()
    {
        dinoMenuNavigationVector = Vector2.zero;
    }
    private void ConfirmSelection(InputAction.CallbackContext obj)
    {
        if(HasSelected)
        {
            OnDinoSelected.Invoke();
            return;
        }
        
        gameDataReference.DinoSelected = hoveredDinoButton.DinoData;
        selectedDino.GetComponentInChildren<Animator>().SetTrigger("Jump");
        HasSelected = true;
        OnDinoSelected.Invoke();
    }

    public void DeselectDino()
    {
        HasSelected = false;
        Destroy(selectedDino);
        gameDataReference.DinoSelected = null;
    }

}
