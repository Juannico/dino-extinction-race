using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCup", menuName = "DinoExtRace/Add Cup Data", order = 52)]
public class CupData : ScriptableObject
{
    /// <summary>
    /// The scene indexes to load the different races
    /// (Best to change it to something like having each race have its data)
    /// </summary>
    public int[] MapsIndexes;

}
