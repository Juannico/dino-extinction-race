﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class RaceManager : MonoBehaviour
{
    [SerializeField] int pointsFirst = 9;
    [SerializeField] int pointsSecond = 6;
    [SerializeField] int pointsThird = 3;
    [SerializeField] int pointsFourth= 1;

    private void Awake()
    {
        //gameDataReference = Resources.Load("GameData") as GameData;
        //uiMan = GameObject.FindGameObjectWithTag("UI Manager").GetComponent<UIManager>();
        //Rect oneP = new Rect(0, 0, 1, 1);
        //players = 1;
        //for (int i = 0; i < players; i++)
        //{
        //    realCameras[i].gameObject.SetActive(true);
        //    hudManagerOnScene[i].gameObject.SetActive(true);
        //}
        //if (players == 1)
        //{
        //    realCameras[0].rect = oneP;
        //    PrepRace();
        //}

        //Finishers = 0;
        //time = 4;
    }
    //void Update()
    //{
    //    time -= Time.deltaTime;
    //    if (m_Dinos.Count > 0)
    //    {
    //        m_Dinos.Sort((d1, d2) => d2.DistanceFormLastCheckpoint.CompareTo(d1.DistanceFormLastCheckpoint));
    //        for (int i = 0; i < m_Dinos.Count; i++)
    //        {
    //            m_Dinos[i].RacePostion = i + 1;
    //        }
            
    //    }
    //    if (Finishers == players)
    //    {
    //        m_DinosFinal = m_Dinos;
    //        m_DinosFinal.Sort((d1, d2) => d2.CupPoints.CompareTo(d1.CupPoints));
    //        StartCoroutine(EndRace());
    //        //wait a moment then laod eithert next race if cup or main menu
    //    }
    //}

    //public void PrepRace()
    //{
    //    //this is hard coded bad but might work
    //    AddDino(gameDataReference.DinoSelected.DinoPrefab, 0, true);
    //    for (int i = 1; i < dinosaurs.Length; i++)
    //    {
    //        AddDino(dinosaurs[i], i, false);
    //    }
    //}

    //public DinoController searchDino(int racePos)
    //{
    //    if(racePos > 0)
    //    return m_Dinos[racePos - 1];
    //    return null;
    //}

    //public void AddDino(GameObject dino, int playerNum, bool isPlayer)
    //{
    //    GameObject temp = Instantiate(dino, contesterStartPoints[playerNum].position, contesterStartPoints[playerNum].rotation, parentContenders);
    //    m_Dinos.Add(temp.GetComponent<DinoController>());
    //    m_Dinos[playerNum].InitDino(trackWaypointsContainer.childCount - 1, playerNum, isPlayer);
    //    if (isPlayer)
    //    {
    //        temp.GetComponent<PlayerController>().localNumber = playerNum;
    //        cameras[playerNum].LookAt = temp.transform;
    //        cameras[playerNum].Follow = temp.transform;
    //    }
    //}

    //public bool StartRace()
    //{
        
    //    if (countdownSound.clip == null)
    //    {
    //        countdownSound.clip = countStart;
    //        countdownSound.Play();
    //    }
    //    if (time < 0)
    //    {
    //        uiMan.ChangeCountdownText("");
    //        if (!bgMusic.isPlaying)
    //        {
    //            bgMusic.Play();
    //        }
    //        for (int i = 0; i < m_Dinos.Count; i++)
    //        {
    //            m_Dinos[i].transform.GetChild(0).GetComponent<Animator>().SetTrigger("Go");
    //            if (!m_Dinos[i].isPlayerControlled)
    //            {
    //                m_Dinos[i].GetComponent<AIController>().enabled = true;
    //            }
    //        }
    //        countdownSound.clip = null;
    //        return true;
    //    }
    //    else if (time <= 1 && time > 0 && countdownSound.clip != countEnd)
    //    {
    //        uiMan.ChangeCountdownText("Go!");
    //        for (int i = 0; i < m_Dinos.Count; i++)
    //        {
    //            m_Dinos[i].transform.GetChild(0).GetComponent<Animator>().SetTrigger("Go");
    //        }
    //        countdownSound.clip = countEnd;
    //        countdownSound.Play();
    //    }
    //    else if (time <= 4 && time > 3.01)
    //    {
    //        uiMan.ChangeCountdownText("3");
    //        for (int i = 0; i < m_Dinos.Count; i++)
    //        {
    //            m_Dinos[i].transform.GetChild(0).GetComponent<Animator>().SetTrigger("Ready");
    //        }
    //    }
    //    else if (time <= 3 && time > 2.01)
    //    {
    //        uiMan.ChangeCountdownText("2");
    //        for (int i = 0; i < m_Dinos.Count; i++)
    //        {
    //            m_Dinos[i].transform.GetChild(0).GetComponent<Animator>().SetTrigger("Set");
    //        }
    //    }
    //    else if (time <= 2 && time > 1.01)
    //    {
    //        uiMan.ChangeCountdownText("1");
    //    }
    //    return false;
    //}

    //public int AwardPoints(int racePos, int playerNum)
    //{
    //    int points = 0;
    //    if (racePos == 1)
    //    {
    //        points = pointsFirst;
    //    }
    //    else if (racePos == 2)
    //    {
    //        points = pointsSecond;
    //    }
    //    else if (racePos == 3)
    //    {
    //        points = pointsThird;
    //    }
    //    else if (racePos == 4)
    //    {
    //        points = pointsFourth;
    //    }
    //    return  points;
    //}

    //public int TotalPlayerPoints(DinoController dino)
    //{
    //    return dino.CupPoints;
    //}

    //IEnumerator EndRace()
    //{
    //    //Show the positions table
    //    uiMan.loadPanelPositions(m_DinosFinal);
    //    Finishers++;
    //    if (gameDataReference.SelectedCup != null)
    //    {
    //        gameDataReference.CurrentCupRaceIndex++;
    //        yield return new WaitForSeconds(8);
    //        if (gameDataReference.CurrentCupRaceIndex >= gameDataReference.SelectedCup.MapsIndexes.Length)
    //            uiMan.LoadScene(0);
    //        else
    //            uiMan.LoadScene(gameDataReference.SelectedCup.MapsIndexes[gameDataReference.CurrentCupRaceIndex]);
    //    }
    //    else
    //    {
    //        yield return new WaitForSeconds(8);
    //        uiMan.LoadScene(0);
    //    }
        
    //}

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///

    [SerializeField] private Database dinosaursDatabase;
    [SerializeField] private Transform racersPositionParentTransform;
    [SerializeField] private Transform raceWaypointsParentTransform;
    [Header("Sound")]
    private AudioSource raceManagerAudioSource;
    [SerializeField] private AudioClip countdownSfx;
    [SerializeField] private AudioClip startRaceSfx;
    [Header("UI")]
    [SerializeField] private HUDManager hudReference; // Might be changed to a set of huds depending on multiplayer....

    private Transform[] racersInitialPositions;
    private PlayerCamera playerCamera;
    private GameData gameDataRef;
    private List<GameObject> instantiatedDinos = new List<GameObject>();

    private void Start()
    {
        gameDataRef = Resources.Load("GameData") as GameData;
        playerCamera = Camera.main.GetComponent<PlayerCamera>();
        raceManagerAudioSource = gameObject.AddComponent<AudioSource>();
        PopulateRacersStartingPoints();
        AddDinosaurRacers();
        StartRaceCountdown();
    }

    private void PopulateRacersStartingPoints()
    {
        if (racersPositionParentTransform == null) { Debug.LogError("No contester position parent transform found, assign it on inspector"); return; }
        racersInitialPositions = new Transform[racersPositionParentTransform.childCount];
        for (int i = 0; i < racersPositionParentTransform.childCount; i++)
        {
            racersInitialPositions[i] = racersPositionParentTransform.GetChild(i);
        }
    }

    private void AddDinosaurRacers()
    {
        int count = 0;
        foreach (var dino in dinosaursDatabase.ScriptableObjectDatabase)
        {
            Dinosaur currentDino = (Dinosaur)dino.Value;
            GameObject dinoInstance = Instantiate(currentDino.DinoPrefab, racersInitialPositions[count].position, racersInitialPositions[count].rotation);
            instantiatedDinos.Add(dinoInstance);
            if (currentDino == gameDataRef.DinoSelected)
            {
                playerCamera.AssignPlayerToCamera(dinoInstance);
                dinoInstance.AddComponent<PlayerController>().enabled = false;
                // init  the player
            }
            else
            {
                var controllerInstance = dinoInstance.AddComponent<AIController>();
                controllerInstance.enabled = false;
                controllerInstance.PopulateWaypoints(raceWaypointsParentTransform);
            }
            count++;
        }
    }

    private void StartRaceCountdown()
    {
        int count = 4;
        StartCoroutine(CoroutineHandler.RepeatFor(() =>
        {
            raceManagerAudioSource.clip = countdownSfx;
            if (count <= 1)
            {
                raceManagerAudioSource.clip = startRaceSfx;
                ActivateDinoForRace();
            }

            raceManagerAudioSource.Play();
            hudReference.ChangeCountdownText(--count);
        }, 4, 1));
    }

    private void ActivateDinoForRace()
    {
        for (int i = 0; i < instantiatedDinos.Count; i++)
        {
            instantiatedDinos[i].GetComponent<BaseController>().enabled = true;
            instantiatedDinos[i].GetComponent<DinoController>().SetChangeIdleState();
        }
    }
}
