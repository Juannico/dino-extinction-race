using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Cinemachine.CinemachineVirtualCamera cameraPlayer;

    public void AssignPlayerToCamera(GameObject playerObject)// the player is assigned to the camera through the player tag
    {
        cameraPlayer.LookAt = playerObject.transform;
        cameraPlayer.Follow = playerObject.transform;
    }
}
