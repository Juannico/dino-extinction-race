﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseController
{
    private PlayerInputActions playerInput;

    private void Awake()
    {
        playerInput = new PlayerInputActions();
    }

    private void OnEnable()
    {
        playerInput.Enable();
    }

    private void OnDisable()
    {
        playerInput.Disable();
    }
    void Start()
    {
        
        dinoController = GetComponent<DinoController>();

        playerInput.PlayerActions.Movement.performed += (ctx) => { dinoController.MovementInputVector = ctx.ReadValue<Vector2>(); };
        playerInput.PlayerActions.Movement.canceled += (ctx) => { dinoController.MovementInputVector = Vector2.zero; };
        playerInput.PlayerActions.Jump.performed += (ctx) => { dinoController.PrepJump(); };
        //playerInput.PlayerActions.Pause.performed += (ctx) => { HandlePauseInput(); };
        //playerInput.PlayerActions.Power.performed += (ctx) => { dino.ThrowPower(); };   
    }
}
