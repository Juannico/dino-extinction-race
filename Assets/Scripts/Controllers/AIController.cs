﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : BaseController
{
    private int currentW;
    private Transform[] waypoints;

    private Vector3 modifiedWaypoint;
    private Vector2 movVector;
    private float currentVelocity;
    private float randomVariation;

    private void Start()
    {
        dinoController = GetComponent<DinoController>();
        currentW = 0;
    }

    public void PopulateWaypoints(Transform waypointsContainer)
    {
        waypoints = new Transform[waypointsContainer.childCount];
        for (int i = 0; i < waypointsContainer.childCount; i++)
        {
            waypoints[i] = waypointsContainer.GetChild(i);
        }
    }

    public void MovementController()
    {
        modifiedWaypoint = waypoints[currentW].position;
        modifiedWaypoint.y = transform.position.y;
        modifiedWaypoint.z = modifiedWaypoint.z + randomVariation;
        movVector = Vector2.zero;
        movVector.y = Vector3.Dot( (modifiedWaypoint - transform.position).normalized,transform.forward.normalized);
        movVector.x = movVector.y < 0.99f 
            ? Mathf.SmoothDamp(movVector.x, -Vector3.Cross((modifiedWaypoint - transform.position).normalized, transform.forward.normalized).y, ref currentVelocity, 1)
            : 0;

        dinoController.MovementInputVector = movVector;
    }

    public void UsePower()
    {
    }

    void Update()
    {
        MovementController();
    }

    public void IncreaseWaypointIndex()
    {
        randomVariation = UnityEngine.Random.Range(-10, 10);
        currentW++;
        if (currentW == waypoints.Length)
        {
            currentW = 0;
        }
    }
}
