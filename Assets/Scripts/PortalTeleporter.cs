using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter : MonoBehaviour
{

	public Transform player;
	public Transform reciever;
	public float delay = 0.2f;

	private bool playerIsOverlapping = false;

	private bool delayActive = false;
	private float time;



    //Update is called once per frame

    void Update()
    {
        if (playerIsOverlapping)
        {
            Vector3 portalToPlayer = player.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            // If this is true: The player has moved across the portal
            if (!delayActive && dotProduct < 0f)
            {
                delayActive = true;
            }

            if (delayActive)
            {
                time += Time.deltaTime;
                if (time > delay)
                {
                    // Teleport him!
                    player.GetComponent<CharacterController>().enabled = false;
                    float rotationDiff = -Quaternion.Angle(transform.rotation, reciever.rotation);
                    //rotationDiff += 180;
                    player.Rotate(Vector3.up, rotationDiff);

                    Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                    player.position = reciever.position + positionOffset;

                    player.GetComponent<CharacterController>().enabled = true;
                    playerIsOverlapping = false;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<DinoController>() != null)
		{
            Vector3 offset = other.transform.position - transform.position;
            offset.y = 0;
			other.transform.position = reciever.position + offset;
		}
	}

	//void OnTriggerExit(Collider other)
	//{
	//	if (other.tag == "Player")
	//	{
	//		playerIsOverlapping = false;
	//	}
	//}
}
