﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour 
{
    [SerializeField] private bool jumpOnThisCheckpoint = false;
    public int numCheckpoint;

    public int NumCheckpoint { get { return numCheckpoint; } }

    public Vector3 PreviousCheckpoint { get; set; }
    public Vector3 NextCheckpoint { get; set; }

    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<AIController>()?.IncreaseWaypointIndex();
        if (jumpOnThisCheckpoint && other.GetComponent<PlayerController>() == null)
        {
            other.GetComponent<DinoController>().PrepJump();
        }
    }
}
