﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

public class GameManager : MonoBehaviour
{
    private GameData gameDataReference;
    public PlayerMenuController[] menuMan;

    private PlayerInputManager inputManager;

    private void Awake()
    {
        gameDataReference = Resources.Load("GameData") as GameData;
        inputManager = GetComponent<PlayerInputManager>();

#if UNITY_EDITOR
        //Print the name of all devices connected while in editor for debugging
        for (int i = 0; i < InputSystem.devices.Count; i++)
        {
            Debug.Log(InputSystem.devices[i]);
        }
#endif
    }

    private void OnEnable()
    {
        inputManager.EnableJoining();
    }

    private void OnDisable()
    {
        inputManager.DisableJoining();
    }

    private void Update()
    {
        InputSystem.onDeviceChange += (device, change) =>
        {
            switch (change)
            {
                case InputDeviceChange.Added:
                    Debug.Log("New device added: " + device);
                    break;

                case InputDeviceChange.Removed:
                    Debug.Log("Device removed: " + device);
                    break;
            }
        };
    }

    public void StartLoadingSceneByIndex(int sceneIndex)
    {
        LoadingScreenManager.LoadScene(sceneIndex);
    }
    public void StartLoadingSceneByName(string sceneName)
    {
        LoadingScreenManager.LoadSceneByName(sceneName);
    }

    public void OnPlayerJoined(PlayerInput joinedPlayer) => Debug.Log($"Player {joinedPlayer.playerIndex} has joined!"); 
}