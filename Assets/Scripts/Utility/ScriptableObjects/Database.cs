﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomDictionaryEntry
{
    public int Key;
    public ScriptableObject Value;
}
[CreateAssetMenu(fileName = "New Database", menuName = "DinoExtRace/Create New Database", order = 52)]
public class Database : ScriptableObject
{
    public CustomDictionaryEntry[] ScriptableObjectDatabase;
    

    public ScriptableObject GetByKey(int SearchKey)
    {
        for (int i = 0; i < ScriptableObjectDatabase.Length; i++)
        {
            if(ScriptableObjectDatabase[i].Key == SearchKey)
            {
                return ScriptableObjectDatabase[i].Value;
            }
        }
        return null;
    }

    public ScriptableObject GetRandomObject(int startingIndex)
    {
        int randomEntry = Random.Range(startingIndex, ScriptableObjectDatabase.Length);
        return ScriptableObjectDatabase[randomEntry].Value;
    }
}
