﻿using System;
using System.Collections;
using UnityEngine;

public class CoroutineHandler : MonoBehaviour
{
    /// <summary>
    /// This method will execute a cooutine passed by parameter after the duration passed by parameter
    /// </summary>
    /// <param name="action"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    public static IEnumerator ExecuteActionAfter(Action action, float duration)
    {
        yield return new WaitForSeconds(duration);
        action?.Invoke();
    }

    /// <summary>
    /// A "virtual" update where an action is repeated until the condition passed by paramter is met
    /// </summary>
    /// <param name="actionToRepeat"></param>
    /// <param name="exitCondition"></param>
    /// <returns></returns>
    public static IEnumerator UpdateUntil(Action actionToRepeat, bool exitCondition)
    {
        while (!exitCondition)
        {
            actionToRepeat?.Invoke();
            yield return null;
        }
    }

    /// <summary>
    /// This method will execute an action a number of times with a set interval of time in between them until the number of executions is met
    /// </summary>
    /// <param name="repeatableAction"></param>
    /// <param name="exitCondition"></param>
    /// <param name="maxTimeBetweenRepeats"></param>
    /// <returns></returns>
    public static IEnumerator RepeatFor(Action repeatableAction, int timesToExecute, float maxTimeBetweenRepeats, bool randomTime = false)
    {
        int countTimes = 0;
        float lastUpdate = 0f;
        float realTimeBetweenRepeats = maxTimeBetweenRepeats;

        //If it is set to be a randomTime then change the time 
        if (randomTime)
        {
            realTimeBetweenRepeats = UnityEngine.Random.Range(0.1f, maxTimeBetweenRepeats);
        }
        while (countTimes < timesToExecute)
        {
            if (realTimeBetweenRepeats < Time.time - lastUpdate)
            {
                repeatableAction?.Invoke();
                //Change the time
                if (randomTime)
                {
                    realTimeBetweenRepeats = UnityEngine.Random.Range(0.1f, maxTimeBetweenRepeats);
                }
                lastUpdate = Time.time;
                countTimes++;
            }
            yield return null;
        }
    }

        /// <summary>
        /// This method will execute an action for a set period of time. Thin of it like an Update that stops running after a certain amount of seconds have passed
        /// </summary>
        /// <param name="repeatableAction"></param>
        /// <param name="exitCondition"></param>
        /// <param name="maxTimeBetweenRepeats"></param>
        /// <returns></returns>
        public static IEnumerator RepeatForSeconds(Action repeatableAction, float timesInSeconds)
        {
            float lastUpdate = Time.time;

            while (timesInSeconds > Time.time - lastUpdate)
            {
                repeatableAction?.Invoke();
                yield return null;
            }

        }
}
