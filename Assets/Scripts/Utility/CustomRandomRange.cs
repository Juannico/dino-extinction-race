﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct IntRange
{
    public int Min;
    public int Max;
    public float Weight;

    public IntRange(int min, int max, float weight)
    {
        Min = min;
        Max = max;
        Weight = weight;
    }
}


public static class CustomRandomRange
{

    public static int Range(params IntRange[] ranges)
    {
        if (ranges.Length == 0) throw new System.ArgumentException("At least one range must be included.");
        if (ranges.Length == 1) return Random.Range(ranges[0].Min, ranges[0].Max);

        float total = 0f;
        for (int i = 0; i < ranges.Length; i++) total += ranges[i].Weight;

        float r = Random.value;
        float s = 0f;

        int cnt = ranges.Length - 1;
        for (int i = 0; i < cnt; i++)
        {
            s += ranges[i].Weight / total;
            if (s >= r)
            {
                return Random.Range(ranges[i].Min, ranges[i].Max);
            }
        }

        return Random.Range(ranges[cnt].Min, ranges[cnt].Max);
    }
}
