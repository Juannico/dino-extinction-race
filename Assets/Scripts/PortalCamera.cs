using UnityEngine;

public class PortalCamera : MonoBehaviour
{

    public Transform portalExit;
    public Transform portalEntrance;
    private Transform playerRef;
    private Camera camaraRef;

    [SerializeField] private Material _PortalCameraMaterial;

    // Use this for initialization
    void Start()
    {
        camaraRef = GetComponent<Camera>();
        //if (camaraRef.targetTexture != null)
        //{
        //    camaraRef.targetTexture.Release();
        //}
        //camaraRef.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        //_PortalCameraMaterial.mainTexture = camaraRef.targetTexture;
    }

    private void FindPlayer()
    {
        if (playerRef == null)
            playerRef = FindObjectOfType<PlayerController>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        FindPlayer();
        if (playerRef == null) return;
        Vector3 playerOffsetFromPortal = playerRef.position - portalEntrance.position;
        //transform.position = portalExit.position + playerOffsetFromPortal;

        float angularDifferenceBetweenPortalRotations = Quaternion.Angle(portalExit.rotation, portalEntrance.rotation);

        Quaternion portalRotationalDifference = Quaternion.AngleAxis(angularDifferenceBetweenPortalRotations, Vector3.up);
        Vector3 newCameraDirection = portalRotationalDifference * playerRef.forward;
        transform.rotation = Quaternion.LookRotation(newCameraDirection, Vector3.up);
    }
}