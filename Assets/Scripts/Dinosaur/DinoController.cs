using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoController : MonoBehaviour
{
    [SerializeField] private Dinosaur dinoData;
    private float groundCheckDistance = 0.1f;
    private float gravityMultiplier = 1.2f;
    private DinoAnimationController dinoAnimController;

    private bool isGrounded = true;
    private float calculatedSpeed;
    /*
    * These values allow the dino know how much distance he has advanced and let the race manager compare positions
    */
    public static int WAYPOINT_VALUE = 1000;
    public static int LAP_VALUE = 100000;

    protected int currentLap;
    private int currentCheckPoint;
    protected int checkPointCounter;
    protected float distanceFromLastCheckpoint;
    protected int num_Waypoints;
    private Vector3 current_CheckPos;
    protected Vector3 prev_CheckPos;
    private Vector3 next_CheckPos;
    protected int racePosition;
    protected bool isRespawning;
    protected bool isInvincible;
    private int cupPoints;
    private Rigidbody rig;
    private GameData gameDataReference;
    private GlobalVariables globalVariables;

    private Vector3 extraGravityForce;
    private Vector3 movementDirection;
    public Vector2 MovementInputVector { get; set; }
    /*
    * Getters and Setters
    * Some related to dinosaur stats and other to gameobjects used for powers
    */
    public float DistanceFormLastCheckpoint { get { return distanceFromLastCheckpoint; } }
    public int RacePostion { get { return racePosition; } set { racePosition = value; } }
    public bool IsRespawning { get { return isRespawning; } set { isRespawning = value; } }

    public int CurrentLap { get { return currentLap; } set { currentLap = value; } }
    public bool IsInvincible { get { return isInvincible; } set { isInvincible = value; } }
    public HUDManager ReferenceHUD { get; set; }
    public Vector3 LastLandPosition { get; set; }
    public int CupPoints { get => cupPoints; set => cupPoints = value; }
    public bool IsGrounded
    {
        get => isGrounded;
        set
        {
            if (isGrounded != value) dinoAnimController?.OnGroundStatusChanged.Invoke(value);
            isGrounded = value;
        }
    }

    public float CalculatedSpeed
    {
        get => calculatedSpeed;
        set
        {
            calculatedSpeed = value;
            dinoAnimController?.OnForwadChanged.Invoke(calculatedSpeed / dinoData.Speed);
        }
    }

    public Dinosaur DinoData { get => dinoData; }

    public void InitDino(int _numWaypoints, int playerN, bool isPlay)
    {
        gameDataReference = Resources.Load("GameData") as GameData;
       
        currentLap = 0;
        currentCheckPoint = 0;
        checkPointCounter = 0;
        distanceFromLastCheckpoint = 0;
        racePosition = -1;
        num_Waypoints = _numWaypoints;
        current_CheckPos = transform.position;
    }

    private void Start()
    {
        rig = GetComponent<Rigidbody>();
        dinoAnimController = transform.GetChild(0).gameObject.AddComponent<DinoAnimationController>();
        if(dinoData.DinoAnimatorOverride != null)
            dinoAnimController.OverrideAnimatorController(dinoData.DinoAnimatorOverride);
        dinoAnimController.OnTriggerJump = Jump;
        globalVariables = Resources.Load("GlobalVariables") as GlobalVariables;
        groundCheckDistance = globalVariables.GroundCheckDistance;
        gravityMultiplier = globalVariables.GravityMultiplier;
    }

    void Update()
    {
        CheckGrounded();
        GetDistanceFromLastCheckPoint();
        HandleAcceleration();
        //RestartPosIfFall();
        CheckWrongWay();
    }

    private void FixedUpdate()
    {
        HandleGroundVelocity();
        HandleRotation();
        HandleFallingMovement();
    }

    private void OnCollisionEnter(Collision collision)
    {
        //dinoData.currentAcceleration = 0;
        //dinoData.currentAcceleration = 0;
    }

    /*
    * Gets the distance from the last checkpoint the player passed through
    */
    public void GetDistanceFromLastCheckPoint()
    {
        distanceFromLastCheckpoint = (transform.position - current_CheckPos).magnitude + currentCheckPoint * WAYPOINT_VALUE
        + currentLap * LAP_VALUE;
    }
    /*
    * Restarts position of dino if it falls below certain
    * TODO Change to restart on checkpoint position
    */
    void RestartPosIfFall()
    {
        if (transform.position.y < -5)
        {
            transform.position = new Vector3(current_CheckPos.x, current_CheckPos.y + 10f, current_CheckPos.z);
        }
    }
    /*
    * checks if the dino is going the wrong direction by using the angle between him and the last check point passed
    */
    public void CheckWrongWay()
    {
        if (ReferenceHUD)
        {
            if (Vector3.Angle(transform.forward, prev_CheckPos - transform.position) < 45)
            {
                ReferenceHUD.wrongWayText.SetActive(true);
            }
            else
                ReferenceHUD.wrongWayText.SetActive(false);
        }
    }

    public void HandleGroundVelocity()
    {
        Vector3 test = transform.forward * CalculatedSpeed;
        rig.velocity = new Vector3(test.x, rig.velocity.y, test.z);
        //transform.position += transform.forward * CalculatedSpeed * Time.fixedDeltaTime;
    }

    public void HandleAcceleration()
    {
        if (MovementInputVector.y <= 0)
            dinoData.currentAcceleration -= Time.deltaTime * dinoData.Acceleration;
        else
            dinoData.currentAcceleration += Time.deltaTime * dinoData.Acceleration;
        dinoData.currentAcceleration = Mathf.Clamp(dinoData.currentAcceleration, 0, dinoData.Acceleration);
        CalculatedSpeed = dinoData.currentSpeed * (dinoData.currentAcceleration / dinoData.Acceleration);
    }

    public void HandleRotation()
    {
        transform.Rotate(new Vector3(0, MovementInputVector.x, 0).normalized * dinoData.currentTurnSpeed * Time.deltaTime);
        Quaternion rotation = Quaternion.Euler(new Vector3(0, MovementInputVector.x * dinoData.currentTurnSpeed * Time.deltaTime, 0).normalized + rig.rotation.eulerAngles);
        rig.rotation = rotation;
    }

    public void CheckGrounded()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
        {
            dinoData.lastLandPosition = transform.position;
            IsGrounded = true;
        }
        else
        {
            IsGrounded = false;
            dinoAnimController.ApplyRootMotion(false);
        }
    }

    void HandleFallingMovement()
    {
        if (IsGrounded) return;
        extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
        rig.AddForce(extraGravityForce, ForceMode.Acceleration);
    }

    public void PrepJump()
    {
        dinoAnimController.OnDinoJump(true);
    }

    public void Jump() => rig.velocity += Vector3.up * dinoData.JumpPower;

    public void SetChangeIdleState()
    {
        StartCoroutine(CoroutineHandler.UpdateUntil(() =>
        {
            dinoAnimController.OnIdleStateChanged.Invoke(dinoAnimController.IdleState - (Time.deltaTime * dinoData.Strength));
        }, dinoAnimController.IdleState <= 0));

    }
}
