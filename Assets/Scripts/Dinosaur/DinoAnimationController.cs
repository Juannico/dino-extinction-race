using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoAnimationController : MonoBehaviour
{
    public delegate void OnDinoBoolParamterChange(bool newBool);
    public delegate void OnDinoFloatParamterChange(float newFloat);
    public delegate void OnDinoActionTrigger();
    public OnDinoBoolParamterChange OnGroundStatusChanged;
    public OnDinoBoolParamterChange OnDinoJump;
    public OnDinoFloatParamterChange OnForwadChanged;
    public OnDinoFloatParamterChange OnIdleStateChanged;
    public OnDinoActionTrigger OnTriggerJump;
    private Animator dinoAnimator;

    public float IdleState { get { return dinoAnimator.GetFloat("IdleState"); } }
    // Start is called before the first frame update
    public void ApplyRootMotion(bool shouldApplyRootMotion) => dinoAnimator.applyRootMotion = shouldApplyRootMotion;
    void Start()
    {
        dinoAnimator = GetComponentInChildren<Animator>();
        OnGroundStatusChanged = (newBool) => { dinoAnimator.SetBool("OnGround", newBool); };
        OnDinoJump = (newBool) => { dinoAnimator.SetBool("Jump", newBool); };
        OnForwadChanged = (newFloat) => { dinoAnimator.SetFloat("Forward", newFloat); };
        OnIdleStateChanged = (newFloat) => { dinoAnimator.SetFloat("IdleState", newFloat); };
    }

    public void TriggerJump()
    {
        OnTriggerJump?.Invoke();
    }

    public void OverrideAnimatorController(AnimatorOverrideController overrider)
    {
        dinoAnimator = GetComponentInChildren<Animator>();
        dinoAnimator.runtimeAnimatorController = overrider;
    }

}
