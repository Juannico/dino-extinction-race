﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Dino Template", menuName = "DinoExtRace/Add Dino Data", order = 52)]
public class Dinosaur : ScriptableObject, ISerializationCallbackReceiver
{
    /// <summary>
    /// Name of the Dino
    /// </summary>
    [Header("Dino Info")]
    public string DinoName;
    public float Speed = 30;
    public float TurnSpeed = 200;
    public float Acceleration = 1;
    public float JumpPower = 10;
    public float Strength = 5;

    [Header("Dino UI")]
    public Sprite DinoIcon;
    [Header("Dino Prefab")]
    public GameObject DinoPrefab;
    public GameObject DinoMenuPrefab;
    [Header("Dino Animator")]
    public AnimatorOverrideController DinoAnimatorOverride;

    [NonSerialized] public float currentSpeed;
    [NonSerialized] public float currentTurnSpeed;
    [NonSerialized] public float currentStrength;
    [NonSerialized] public float currentAcceleration;
    [NonSerialized] public float currentJumpPower;
    [NonSerialized] public Vector3 lastLandPosition;

    public void RestartSpeed()
    {
        currentSpeed = Speed;
    }

    public void RestartStrength()
    {
        currentStrength = Strength;
    }

    public void OnAfterDeserialize()
    {
        RestartSpeed();
        RestartStrength();
        currentTurnSpeed = TurnSpeed;
        currentAcceleration = 0;
        currentJumpPower = JumpPower;
        lastLandPosition = Vector3.zero;
    }

    public void OnBeforeSerialize() {}
}
