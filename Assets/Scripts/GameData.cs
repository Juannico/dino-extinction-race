using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGameData", menuName = "DinoExtRace/Create Game Data", order = 51)]
public class GameData : ScriptableObject, ISerializationCallbackReceiver
{
    //Selected cup to play, can be null
    [SerializeField] private GameMode selectedGameMode;
    [SerializeField] private CupData selectedCup;
    [NonSerialized] private int currentCupRaceIndex;
    [SerializeField] private int SelectedRaceSceneIndex;

    [Header("Player Data")]

    [SerializeField] private Dinosaur dinoSelected;
    [NonSerialized] private int cupPoints;

    public int CurrentCupRaceIndex { get => currentCupRaceIndex; set => currentCupRaceIndex = value; }
    public int CupPoints { get => cupPoints; set => cupPoints = value; }
    public GameMode SelectedGameMode { get => selectedGameMode; set => selectedGameMode = value; }
    public CupData SelectedCup { get => selectedCup; set => selectedCup = value; }
    public Dinosaur DinoSelected { get => dinoSelected; set => dinoSelected = value; }

    public void OnAfterDeserialize()
    {

    }

    public void OnBeforeSerialize()
    {

    }
}
