﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PowerUpAI : MonoBehaviour
{
    private NavMeshAgent agent;
    public int CurrentIndex { get; set; }
    public DinoController DinoRef { get; set; }
    private Transform[] path;
    private PowerUps powerRef;

    // Update is called once per frame
    void Update()
    {
        if (agent.enabled)
        {
            if (CurrentIndex < path.Length && (agent.remainingDistance <= 5f || !agent.hasPath))
            {
                agent.destination = path[CurrentIndex].position;
                CurrentIndex++;
            }
            else if (CurrentIndex == path.Length)
            {
                CurrentIndex = 0;
            }
        }
    }

    public void Initialization(Transform[] nPath, int index, DinoController refe, PowerUps pUp, Vector3 origin)
    {
        agent = GetComponent<NavMeshAgent>();
        DinoRef = refe;
        path = nPath;
        CurrentIndex = index;
        powerRef = pUp;
        transform.position = origin;
        agent.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dinosaur"))
        {
            if (other.GetComponent<DinoController>() == DinoRef)
            {
                return;
            }
            if (powerRef.GetPowerType() == PowerUps.PowerBehaviour.POSITION_CHANGER)
            {
                powerRef.DoubleEffect(DinoRef, other.GetComponent<DinoController>());
            }
            else
                powerRef.InstantEffect(other.GetComponent<DinoController>());
            Destroy(gameObject);
        } 
    }
}
