﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsPick : MonoBehaviour
{
    public Transform powerUpHolder;
    private PowerUps[] possiblePowerUps;
    public bool isAutomatic;

    private IntRange[] firstRange = { new IntRange(0,2, 95f), new IntRange(2, 2, 5f) };
    private IntRange[] secondRange = { new IntRange(0, 10, 90.5f), new IntRange(10, 10, 5f), new IntRange(11, 11, 4.5f) };
    private IntRange[] thirdRange = { new IntRange(2, 10, 80f), new IntRange(10, 10, 15f), new IntRange(11, 11, 5f) };
    private IntRange[] fourthRange = { new IntRange(9, 9, 30f), new IntRange(10, 10, 65f), new IntRange(11, 11, 5f) };

    protected GameObject partis;

    void Start()
    {
        possiblePowerUps = new PowerUps[powerUpHolder.childCount];
        for (int i = 0; i < powerUpHolder.childCount; i++)
        {
            possiblePowerUps[i] = powerUpHolder.GetChild(i).GetComponent<PowerUps>();
        }
        
        if (!isAutomatic)
        {
            partis = gameObject.transform.GetChild(1).gameObject;
        }
    }

    void OnTriggerEnter(Collider d)
    {
        if (d.CompareTag("Dinosaur") && possiblePowerUps.Length > 0)
        {
            int generator = PositionPowernumberGenerator(d.GetComponent<DinoController>());
            if (!isAutomatic)
            {
                //d.GetComponent<DinoController>().StorePower(possiblePowerUps[generator]);
                StartCoroutine(disthenen());
            }
        }
    }

    int PositionPowernumberGenerator(DinoController dino)
    {
        int generator = 0;
        if (dino.RacePostion == 1)
        {
            generator = CustomRandomRange.Range(firstRange);
        }
        else if (dino.RacePostion >= 2 && dino.RacePostion <= 4)
        {
            generator = CustomRandomRange.Range(secondRange);
        }
        else if (dino.RacePostion >= 5 && dino.RacePostion <= 7)
        {
            generator = CustomRandomRange.Range(thirdRange);
        }
        else if (dino.RacePostion == 8)
        {
            generator = CustomRandomRange.Range(fourthRange);
        }
        return generator;
    }

    IEnumerator disthenen()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<SphereCollider>().enabled = false;
        partis.GetComponentInChildren<ParticleSystem>()?.Play();
        yield return new WaitForSeconds(2f);
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<SphereCollider>().enabled = true;
    }
}
