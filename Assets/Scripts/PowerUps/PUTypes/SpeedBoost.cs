﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : PowerUps
{
    public int additionalSpeed;
    public override PowerBehaviour GetPowerType() { return PowerBehaviour.NITRO; }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }

    IEnumerator Tick(DinoController d)
    {
        //d.speedParticles.Play();
        //d.Rocket.SetActive(true);
        //d.variableSpeed += additionalSpeed;
        yield return new WaitForSeconds(duration);
        //d.variableSpeed = d.BaseSpeed;
        //d.Rocket.SetActive(false);
        //d.speedParticles.Stop();
    }
}
