﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceFieldPower : PowerUps
{
    public int additionalSpeed;
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.FORCE_FIELD;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }

    IEnumerator Tick(DinoController d)
    {
        //d.speedParticles.Play();
        //d.IsInvincible = true;
        //d.variableSpeed += additionalSpeed;
        //d.Strength += 10;
        //d.ForceField.SetActive(true);
        yield return new WaitForSeconds(duration);
        //d.ForceField.SetActive(false);
        //d.Strength -= 10;
        //d.IsInvincible = false;
        //d.variableSpeed = d.BaseSpeed;
        //d.speedParticles.Stop();
    }
}
