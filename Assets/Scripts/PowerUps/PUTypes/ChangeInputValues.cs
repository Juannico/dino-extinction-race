﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeInputValues : PowerUps
{
    public bool invertHorizontal;
    public bool invertAccel;
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.THROW_FOLLOW_TRACK;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }
    IEnumerator Tick(DinoController d)
    {
        //if (invertAccel)
        //{
        //    d.variableSpeed *= -1;
        //}
        //if (invertHorizontal)
        //{
        //    d.TurnSpeed *= -1;
        //}
        yield return new WaitForSeconds(duration);
        //d.variableSpeed = d.BaseSpeed;
        //d.TurnSpeed *= -1; 
    }
}
