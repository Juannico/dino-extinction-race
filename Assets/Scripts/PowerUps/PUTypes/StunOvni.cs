﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunOvni : PowerUps
{
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.THROW_FOLLOW_TRACK;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }

    IEnumerator Tick(DinoController d)
    {
        //d.variableSpeed = 0;
        //d.Ovni.SetActive(true);
        yield return new WaitForSeconds(duration);
        //d.variableSpeed = d.BaseSpeed;
        //d.Ovni.gameObject.SetActive(false);
    }
}
