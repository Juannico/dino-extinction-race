﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowChicken : PowerUps
{
    public float speedSubstraction;

    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.THROW_FOLLOW_TRACK;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }


    IEnumerator Tick(DinoController d)
    {
        //d.variableSpeed -= speedSubstraction;
        //DinosaurMesh[] dinoMeshes = d.dinosaurModel.GetComponentsInChildren<DinosaurMesh>();
        //for (int i = 0; i < dinoMeshes.Length; i++)
        //{
        //    dinoMeshes[i].gameObject.SetActive(false);
        //}
        //d.Chicken.SetActive(true);
        yield return new WaitForSeconds(duration);
        //d.Chicken.SetActive(false);
        //for (int i = 0; i < dinoMeshes.Length; i++)
        //{
        //    dinoMeshes[i].gameObject.SetActive(true);
        //}
        //d.variableSpeed = d.BaseSpeed;
    }
}
