﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldPower : PowerUps
{
    public float timeToArm;
    public Vector3 offsetFromThrower;
    public Vector3 RotationFromThrower;
    public GameObject objectToThrow;

    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.FIELD_POWER;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        GameObject temp = Instantiate(objectToThrow, d.transform.localPosition + offsetFromThrower, Quaternion.Euler(RotationFromThrower + d.transform.rotation.eulerAngles));
        temp.SetActive(true);
        StartCoroutine(ArmCollider(timeToArm, temp.GetComponent<Collider>()));
    }

    IEnumerator ArmCollider(float time, Collider col)
    {
        yield return new WaitForSeconds(time);
        col.enabled = true;
    }
}
