﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowFirst : PowerUps
{
    public GameObject objectToThrow;
    public ParticleSystem explosion;
    public int radius;
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.AIM_FIRST;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        objectToThrow.SetActive(true);
        objectToThrow.transform.position = new Vector3(d.gameObject.transform.position.x, d.gameObject.transform.position.y + 30, d.gameObject.transform.position.z);
        objectToThrow.transform.parent = d.transform;
        StartCoroutine(Tick(d));
    }

    IEnumerator Tick(DinoController d)
    {
        d.transform.GetChild(0).GetComponent<Animator>().SetLayerWeight(2, 1);
        while (Vector3.Distance(objectToThrow.transform.position, d.transform.position) > 0.2f)
        {
            objectToThrow.transform.position = Vector3.MoveTowards(objectToThrow.transform.position, d.transform.position, 20f * Time.deltaTime);
            yield return new WaitForSeconds(Time.deltaTime);
        }

        objectToThrow.transform.parent = gameObject.transform;
        objectToThrow.gameObject.SetActive(false);
        explosion.transform.position = d.transform.position;
        explosion.Play();
        Vector3 center = d.transform.position;
        Collider[] hitColl = Physics.OverlapSphere(center, radius);
        for (int i = 0; i < hitColl.Length; i++)
        {
            if (hitColl[i].CompareTag("Dinosaur"))
            {
                if (!hitColl[i].GetComponent<DinoController>().IsInvincible)
                {
                    StartCoroutine(Stunned(hitColl[i].GetComponent<DinoController>()));
                }
            }
        }
        //explosion and aoe
        if (!d.IsInvincible)
        {
            StartCoroutine(Stunned(d));
        }
    }

    IEnumerator Stunned(DinoController dino)
    {
        //dino.variableSpeed = 0;
        //dino.IsInvincible = true;
        //dino.transform.GetChild(0).GetComponent<Animator>().SetLayerWeight(2, 0);
        //dino.transform.GetChild(0).GetComponent<Animator>().SetBool("HitFall", true);
        yield return new WaitForSeconds(duration);
        //dino.transform.GetChild(0).GetComponent<Animator>().SetBool("HitFall", false);
        //dino.IsInvincible = false;
        //dino.variableSpeed = dino.BaseSpeed;
    }
}
