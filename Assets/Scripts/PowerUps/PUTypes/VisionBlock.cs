﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionBlock : PowerUps
{
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.THROW_FOLLOW_TRACK;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }

    IEnumerator Tick(DinoController d)
    {
        if (d.ReferenceHUD != null)
        {
            d.ReferenceHUD.visionBlock.gameObject.SetActive(true);
            yield return new WaitForSeconds(duration);
            d.ReferenceHUD.visionBlock.gameObject.SetActive(false);
        }
    }
}
