﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePosition : PowerUps
{
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.POSITION_CHANGER;
    }

    public override void DoubleEffect(DinoController origin, DinoController dest)
    {
        base.DoubleEffect(origin, dest);
        Vector3 tempPos = origin.transform.position;
        origin.transform.position = dest.transform.position;
        dest.transform.position = tempPos;
    }

}
