﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperStregth : PowerUps
{
    public int addStrength;
    public override PowerBehaviour GetPowerType()
    {
        return PowerBehaviour.SUPER_STREGTH;
    }

    public override void InstantEffect(DinoController d)
    {
        base.InstantEffect(d);
        StartCoroutine(Tick(d));
    }

    IEnumerator Tick(DinoController d)
    {
        //d.FootballHelmet.SetActive(true);
        //d.Strength += addStrength;
        yield return new WaitForSeconds(duration);
        //d.Strength -= addStrength;
        //d.FootballHelmet.SetActive(false);
    }
}
