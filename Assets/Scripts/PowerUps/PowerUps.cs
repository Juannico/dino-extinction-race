﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class PowerUps : MonoBehaviour
{
    public int powerNumber;
    public float duration;
    public AudioClip powerUpAudio;
    public ParticleSystem particles;
    public GameObject launchObject;


    public enum PowerBehaviour
    {
        NITRO,
        SUPER_STREGTH,
        FIELD_POWER,
        FORCE_FIELD,
        AIM_FIRST,
        INPUT_CHANGER,
        POSITION_CHANGER,
        VISION_BLOCK,
        THROW_FOLLOW_TRACK
    }

    public abstract PowerBehaviour GetPowerType();
    protected ParticleSystem m_ParticleSpawned;

    public virtual void InitAI(Transform[] path, int index, DinoController dino, PowerUps power, Vector3 origin)
    {
        GameObject obj = Instantiate(launchObject);
        obj.SetActive(true);
        obj.GetComponent<PowerUpAI>().Initialization(path, index, dino, power,origin);
    }

    public virtual void InstantEffect(DinoController d)
    {
        if (powerUpAudio != null)
        {
            d.gameObject.GetComponent<AudioSource>().clip = powerUpAudio;
            d.gameObject.GetComponent<AudioSource>().Play();
        }
    }

    public virtual void DoubleEffect(DinoController origin, DinoController dest)
    {

        if (powerUpAudio != null)
        {
            dest.gameObject.GetComponent<AudioSource>().clip = powerUpAudio;
            dest.gameObject.GetComponent<AudioSource>().Play();
        }
    }
}
