using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionChange : MonoBehaviour
{
    [Header("Components")]
    [Tooltip("Agregar el material Position_Graph")] 
    [SerializeField] Material PosMaterial;

    [Header("Configuration")]
    [Tooltip("Array de los colores que va a tener cada posición (en caso de que se borren revisar documentación anexa)")] 
    [SerializeField] Color[] Colors;
    [Tooltip("Array de las posiciones en las que se encuentra cada numero dentro del Shader (en caso de que se borren revisar documentación anexa)")]
    [SerializeField] Vector2[] Position;

    private int Pos = 0;

    void Start()
    {
        PosMaterial.SetVector("_Offset", Position[Pos]);
    }


    void Update()
    {
        OnPositionChange();
    }


    /*Se debe cambiar el Input por el metodo que se use para reconocer la posicion del jugador, "_Offset" es la propiedad 
      que permite mover la textura a la posicion deseada y "_Color" cambia el color respectivamente. */
    void OnPositionChange ()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Pos++;
            PosMaterial.SetVector("_Offset", Position[Pos]);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Pos--;
            PosMaterial.SetVector("_Offset", Position[Pos]);
        }
        switch (Pos)
        {
            case 0:
                PosMaterial.SetColor("_Color", Colors[0]);
                break;
            case 1:
                PosMaterial.SetColor("_Color", Colors[1]);
                break;
            case 2:
                PosMaterial.SetColor("_Color", Colors[2]);
                break;
            case 3:
                PosMaterial.SetColor("_Color", Colors[3]);
                break;
            case 4:
                PosMaterial.SetColor("_Color", Colors[4]);
                break;
            case 5:
                PosMaterial.SetColor("_Color", Colors[5]);
                break;
            case 6:
                PosMaterial.SetColor("_Color", Colors[6]);
                break;
            case 7:
                PosMaterial.SetColor("_Color", Colors[7]);
                break;
        }
    }
}
