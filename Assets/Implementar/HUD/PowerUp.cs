using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;


public class PowerUp : MonoBehaviour
{
    [Header("Components")]
    [Tooltip ("Agregar los sprites de PowerUps")]
    [SerializeField] Sprite[] PowerUps;
    [Tooltip ("Agregar el componente Image que va a representar el PowerUp en el HUD")]
    [SerializeField] Image Powerup;


    [Header("Configuration")]
    [Tooltip ("Definir el tiempo de animación inicial")]
    [SerializeField] float AnimationTimeOn = 0.3f;
    [Tooltip("Definir el tiempo de animación final")]
    [SerializeField] float AnimationTimeOff = 0.2f;

    private int Delay = 1;
    private int Loop = 2;
    void Start()
    {
        Powerup.gameObject.SetActive(false);
    } 


    /*Se debe cambiar por un trigger o la forma en la que se recojan los PowerUps */
     
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Poder recogido");
            Powerup.GetComponent<EasyTween>()?.ChangeSetState(true);
            Powerup.GetComponent<EasyTween>()?.OpenCloseObjectAnimation();
            Powerup.GetComponent<EasyTween>()?.SetAnimatioDuration(AnimationTimeOn);
            ChangePowerUp();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("Poder lanzado");
            Powerup.GetComponent<EasyTween>()?.ChangeSetState(false);
            Powerup.GetComponent<EasyTween>()?.OpenCloseObjectAnimation();
            Powerup.GetComponent<EasyTween>()?.SetAnimatioDuration(AnimationTimeOff);
        }
    }


    /* Anima el cambio random de cada PowerUp del Array y define uno final */
    public async void ChangePowerUp()
    {
        Powerup.gameObject.SetActive(true);
        for (int i = 0; i < Loop; i++)
        {
            foreach (Sprite item in PowerUps)
            {
                await Task.Delay((Delay * 1000) / 10);
                Powerup.sprite = item;
            }
        }
        int a = Random.Range(0, PowerUps.Length);
        Powerup.sprite = PowerUps[a];
    }
}
